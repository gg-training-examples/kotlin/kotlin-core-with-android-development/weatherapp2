package com.groundgurus.weatherapp2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.groundgurus.weatherapp2.weather.ApiService
import com.groundgurus.weatherapp2.weather.Climate
import com.groundgurus.weatherapp2.weather.WeatherIconService
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    lateinit var queryEditText: EditText
    lateinit var searchButton: Button
    lateinit var weatherInTextView: TextView
    lateinit var mainTextView: TextView
    lateinit var descriptionTextView: TextView
    lateinit var iconImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        val climate = ApiService.getClimate(queryEditText.text.toString())
        updateUI(climate)
    }

    private fun init() {
        queryEditText = findViewById(R.id.queryEditText)
        searchButton = findViewById(R.id.searchButton)
        weatherInTextView = findViewById(R.id.weatherInTextView)
        mainTextView = findViewById(R.id.mainTextView)
        descriptionTextView = findViewById(R.id.descriptionTextView)
        iconImageView = findViewById(R.id.iconImageView)

        searchButton.setOnClickListener {
            val query = queryEditText.text.toString()
            val climate = ApiService.getClimate(query)
            updateUI(climate)
        }
    }

    private fun updateUI(climate: Call<Climate>) {
        climate.enqueue(object : Callback<Climate> {
            override fun onFailure(call: Call<Climate>, t: Throwable) {
                Log.d("MainActivity", t.message)
            }

            override fun onResponse(call: Call<Climate>, response: Response<Climate>) {
                if (response.isSuccessful) {
                    val resp = response.body() as Climate

                    weatherInTextView.text =
                        getString(R.string.weather_in, resp.name)
                    val weather = resp.weather[0]
                    mainTextView.text = weather.main
                    descriptionTextView.text = weather.description

                    Picasso.get().load(WeatherIconService.getIconUrl(weather.icon))
                        .into(iconImageView)
                }
            }
        })
    }
}
