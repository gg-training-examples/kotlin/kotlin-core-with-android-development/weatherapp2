package com.groundgurus.weatherapp2.weather

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("weather")
    fun getClimate(
        @Query("q") query: String,
        @Query("appid") apiKey: String
    ): Call<Climate>
}