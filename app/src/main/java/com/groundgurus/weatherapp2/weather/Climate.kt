package com.groundgurus.weatherapp2.weather

data class Climate(
    val name: String,
    val weather: List<Weather>
)

data class Weather(
    val main: String,
    val description: String,
    val icon: String
)