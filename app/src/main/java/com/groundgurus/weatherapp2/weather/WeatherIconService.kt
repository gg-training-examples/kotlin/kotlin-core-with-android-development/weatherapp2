package com.groundgurus.weatherapp2.weather

object WeatherIconService {

    fun getIconUrl(icon: String): String {
        return "https://openweathermap.org/img/w/$icon.png"
    }
}