package com.groundgurus.weatherapp2.weather

import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiService {
    private const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
    private const val API_KEY = "96c20bd7450059c8d1ba71d6a7dc73a4"

    fun getClimate(query: String): Call<Climate> {
        val gson = GsonBuilder().setLenient().create()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        val weatherService = retrofit.create(WeatherService::class.java)

        return weatherService.getClimate(query, API_KEY)
    }
}